## Add rate adn comment to point

    method: POST
    url: /add_rate/
    data: 
        {
            "value": number[1-10],
            "point_id": number,
            "comment": string *not_require
        }
    response:
        {
          "data": {
            "comment": string,
            "id": number,
            "value": number
          },
          "msg": string,
          "status": bool
        }