## Get points in database
    method: GET
    url: /
    data: {}
    response:
        [
          {
            "id": 1,
            "lat": 50.64896,
            "lng": 30.28886,
            "name": "afccab5c-8121-4738-9825-93bb1fc29074",
            "rating": 8.333333333333334
          },
          {
            "id": 2,
            "lat": 50.22717,
            "lng": 30.62363,
            "name": "28f00ad7-d47c-451c-9739-0d297cd1b51b",
            "rating": 4
          },
          ...
          {
            "id": 3,
            "lat": 50.23076,
            "lng": 30.09657,
            "name": "300773b2-4913-442b-898f-87582c0a7f09",
            "rating": 3
          }
        ]
    filters:
        lat - latitude of search point - number
        lng - longtitude of search point - number
        lat and lng work only together
        radius - radius in kilometers where you search point - number
        example - /?lat=50.1&lng=30.1&radius=0.5  

## Create new point

    method: POST
    url: /
    data: 
        {
            "lat": number,
            "lng": number,
            "name": string
        }
    response:
        {
            'status': bool, 
            'data': point data, 
            'msg': string
        } 