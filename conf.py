import sys
from recommonmark.parser import CommonMarkParser
sys.path.insert()

source_parsers = {
    '.md': CommonMarkParser,
}

source_suffix = ['.rst', '.md']